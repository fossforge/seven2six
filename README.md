# INTRODUCTION

The Java 7 language contains many awesome features, yet the only class file version that the javac compiler lets us compile Java 7 source into (with "-target"), is Java 7's.

This project is a Maven plugin that backports classes to Java 6 to work around that limitation. You compile Java 7 code as per usual, then post-process it with this plugin.



# STATUS OF JAVA 7 FEATURES

1. invokedynamic

    Not supported - the plugin will report an error and stop the build.

2. Binary literals (0b1010) and underscores in numeric literals (1_000_000)

    Supported - javac already transforms them into the actual numbers at compile-time.

3. Strings in switch

    Supported - javac already compiles it as a switch on the string's hashcode, which runs on earlier versions.

4. Type Inference for Generic Instance Creation (a.k.a. "diamonds")

    Supported - syntactic sugar.

5. Catching Multiple Exception Types and Rethrowing Exceptions with Improved Type Checking

    Supported.

6. try-with-resources

    Supported, but any suppressed exceptions will be lost. See the details section below.



# FAQ

Q: Does this plugin backport the Java 7 API too?

A: No.

Q: Does this plugin verify that no Java 7-only APIs are being used?

A: No, but have a look at the animal-sniffer plugin for that. If you are using the 2 together, make sure this plugin runs before the animal-sniffer plugin, otherwise you'll get errors about Throwable.addSuppressed() which this plugin would have deleted when run first.

Q: Does this plugin backport all my dependencies or just my code?

A: Just your code.

Q: Why change bytecode after compilation? Why not patch javac itself to output bytecode for Java < 7?

A: I did consider patching javac, and it wouldn't even be that difficult, but javac is compiled as and compiled into part of an entire JDK which would need to be distributed with it, and which is platform-specific. Even once you download it, adding a new compiler needs a lot of work to integrate into your project, and is extremely disruptive and invasive to existing toolchains (eg. you'd need a new maven-compiler-plugin backend).



# DETAILS

For the try-with-resources statement, Java 7 compiles:

```
try (InputStream is = factoryMethodThatCanThrow()) {
    is.methodThatCanThrow();
}
```

into:

```
final InputStream is = factoryMethodThatCanThrow();
Throwable primaryException = null;
try {
    is.methodThatCanThrow();
} catch (Throwable t) {
    primaryException = t;
    throw t;
} finally {
    if (is != null) {
        if (primaryException != null) {
            try {
                is.close();
            } catch (Throwable t) {
                primaryException.addSuppressed(t);
            }
        } else {
            is.close();
        }
    }
}
```

There is 2 problems backporting this:
1. Earlier versions of Java don't have the Throwable.addSuppressed() method.
2. Even the expanded code won't compile on earlier versions of javac, because the "rethrowing exceptions with improved type checking" feature is missing.

Fortunately we work around problem (2) by compiling on Java 7, so we don't use the javac from earlier versions.

Problem (1) can be solved in one of the following ways:
- Change the Throwable class in the class library of earlier versions. That's not likely to happen in general, but people have done just that on Android (https://github.com/yareally/Java7-on-Android and http://www.informit.com/articles/article.aspx?p=1966024). I prefer something more portable.
- Store the suppressed exceptions in some kind of parameter that gets passed around. Ugly, and changes the API.
- Store the suppressed exceptions in a thread-local variable. Would work, without needing any parameter to get passed around. But managing that thread-local variable becomes a problem: client code has to remember clear it after every handled exception, and client and server have to agree on what variable to use.
- Our solution: delete the call to Throwable.addSuppressed() at the bytecode level, resulting in the bytecode equivalent of the following, which is valid on JVMs < 7:

```
final InputStream is = factoryMethodThatCanThrow();
Throwable primaryException = null;
try {
    is.methodThatCanThrow();
} catch (Throwable t) {
    primaryException = t;
    throw t;
} finally {
    if (is != null) {
        if (primaryException != null) {
            try {
                is.close();
            } catch (Throwable t) {
            }
        } else {
            is.close();
        }
    }
}
```

In other words, if there's a pending exception when the finally block is entered, that exception is always the one that will get thrown, and any suppressed exceptions from close() methods will be lost. If there is no pending exception, then the first close() exception, if any, is the one that will eventually be thrown, and other will be lost (because when there are multiple resources in the try-with-resources block, they get compiled into individual try-catch-finally blocks, each nested in the previous resource's try block body).

So this behaves exactly like it would in Java 7, but with no possibility of accessing the suppressed exceptions.

Note that even if you call Throwable.addSuppressed() manually, in your code, that call will be deleted.
