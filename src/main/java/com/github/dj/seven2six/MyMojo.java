package com.github.dj.seven2six;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * Goal which backports Java 7 bytecode to Java 6. 
 *
 * @goal seven2six
 * 
 * @phase process-classes
 */
public class MyMojo
    extends AbstractMojo {
    
    private final static byte[] classHeader = new byte[] {
        (byte)0xca, (byte)0xfe, (byte)0xba, (byte)0xbe
    };
    
    /**
     * Location of the "target/classes" directory.
     * @parameter expression="${project.build.outputDirectory}"
     * @required
     */
    private File outputDirectory;
    
    public void setOutputDirectory(final File outputDirectory) {
        this.outputDirectory = outputDirectory.getAbsoluteFile();
    }

    public void execute()
        throws MojoExecutionException {
        File f = outputDirectory;

        if (!f.exists()){
            f.mkdirs();
        }

        try {
            findAndBackportClasses(outputDirectory);
        } catch (final IOException ioException) {
            throw new MojoExecutionException("Error", ioException);
        }
    }
    
    private void findAndBackportClasses(final File directory) throws IOException {
        for (final File file : directory.listFiles()) {
            getLog().debug("Examining " + file);
            if (file.isDirectory()) {
                findAndBackportClasses(file);
                continue;
            }
            if (isClass(file)) {
                backportClass(file);
            }
        }
    }
    
    private boolean isClass(final File file) throws IOException {
        final byte[] first4Bytes = new byte[4];
        try (FileInputStream classStream = new FileInputStream(file)) {
            for (int bytesRead = 0; bytesRead < 4;) {
                int r = classStream.read(first4Bytes, bytesRead, 4 - bytesRead);
                if (r < 0) {
                    break;
                }
                bytesRead += r;
            }
        }
        return Arrays.equals(first4Bytes, classHeader);
    }
    
    private void backportClass(final File file) throws IOException {
        getLog().info("Backporting " + file);
        ClassNode classNode = new ClassNode();
        final ClassReader classReader;
        try (FileInputStream classStream = new FileInputStream(file)) {
            classReader = new ClassReader(classStream);
        }
        classReader.accept(classNode, 0);
        
        classNode.version = 50;
        
        for (int i = 0; i < classNode.methods.size(); i++) {
            MethodNode method = (MethodNode) classNode.methods.get(i);
            backportMethod(classNode.name, method);
        }
        
        final ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        classNode.accept(classWriter);
        final byte[] transformedClass = classWriter.toByteArray();
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(transformedClass);
        }
    }
    
    private void backportMethod(final String className, final MethodNode method) throws IOException {
        for (int j = 0; j < method.instructions.size(); j++) {
            AbstractInsnNode instruction = method.instructions.get(j);
            
            if (instruction.getOpcode() == Opcodes.INVOKEDYNAMIC) {
                getLog().error("The invokedynamic instruction cannot be backported, aborting build " +
                        "(method " + method.name + " of class " + className + ")");
                throw new IOException("invokedynamic");
            }
            
            if (instruction.getType() == InsnNode.METHOD_INSN) {
                MethodInsnNode methodNode = (MethodInsnNode)instruction;
                // To backport try-with-resources, we need to remove
                // the call to the Throwable.addSuppressed() method
                // which didn't exist before Java 7.
                if (methodNode.owner.equals("java/lang/Throwable") &&
                        methodNode.name.equals("addSuppressed") &&
                        methodNode.desc.equals("(Ljava/lang/Throwable;)V")) {
                    // Stack contents before Throwable.addSuppressed():
                    //     the throwable's objectRef
                    //     the Ljava/lang/Throwable arg's objectRef
                    //     ...
                    //
                    // Stack contents after  Throwable.addSuppressed():
                    //     ...
                    //
                    // So we need to pop 2 stack slots -> replace it with POP2
                    method.instructions.set(instruction, new InsnNode(Opcodes.POP2));
                }
            }
        }
    }
}
